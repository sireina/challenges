
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactDOM from 'react-dom';
import { Circle } from 'rc-progress';
import 'rc-progress/assets/index.css';

class LoadingIndicator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      percent: 0,
      currentColor: 0,
      colors: ['#8CB8DF', '#3276B1', '#23537C'],
    };
  }

  componentDidMount = () => {
  	if (this.props.determinate) {
  		this.loadDeterminate();
  		this.changeColor();
  	} else {
  		this.increase();
  	}
  }

  componentWillUnmount = () => {
  	if (this.determinateTimeout) {
  		clearTimeout(this.determinateTimeout);
  		clearTimeout(this.colorChangeTimeout);
  	}

  	if (this.indeterminateTimeout) {
  		clearTimeout(this.indeterminateTimeout);
      clearTimeout(this.colorChangeTimeout);
  	}
  }

  loadDeterminate = (ms) => {
  	let time = ms ? ms : 1200;
		const percent = Math.round(time / 150);

		if (percent >= 100) {
			return;
		} else {
			time *= 1.1;

			this.setState({
				percent: percent,
			});

			this.determinateTimeout = setTimeout(() => this.loadDeterminate(time), time);
			return;
		}
  }

  increase = () => {
  	const percent = this.state.percent + 1;

  	if (percent > 100) {
  		this.restart();
  		return;
  	}

  	this.setState({
  		percent: percent,
  	});

  	this.indeterminateTimeout = setTimeout(this.increase, 20);
  }

  restart = () => {
  	clearTimeout(this.indeterminateTimeout);
  	const currentColor = this.state.currentColor < this.state.colors.length - 1 ? this.state.currentColor + 1 : 0;

  	this.setState({
  		percent: 0,
  		currentColor: currentColor,
  	});

  	this.increase();
  }

  changeColor = () => {
  	const currentColor = this.state.currentColor < this.state.colors.length - 1 ? this.state.currentColor + 1 : 0;

  	this.setState({
  		currentColor: currentColor,
  	});

  	this.colorChangeTimeout = setTimeout(this.changeColor, 900);
  }

  render() {
  	return (
  		<div className={`clearfix loader-wrapper${this.props.small ? " loader-sm" : ""}`}>
	  		{!this.props.small ?
	  			<div className="loader-text">
	  			{this.props.determinate ?
	  				<span><h1>{this.state.percent}</h1><h3>%</h3></span>
	  			:
	  				<h4>{this.props.text}</h4>
	  			}
	  			</div>
	  		:
	  			<span className="sr-only">Loading content</span>
	  		}
	  		<Circle
	  			percent={this.state.percent}
	  			strokeWidth={this.props.small ? 20 : 10}
	  			strokeColor={this.state.colors[this.state.currentColor]}
	  			strokeLinecap="butt"
	  		/>
  		</div>
		);
  }
};

LoadingIndicator.propTypes = {
	text: PropTypes.string,
	small: PropTypes.bool,
	determinate: PropTypes.bool,
};

LoadingIndicator.defaultProps = {
	text: "Loading",
	small: false,
	determinate: false,
}

export default LoadingIndicator;
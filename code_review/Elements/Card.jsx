
import React from 'react';
import PropTypes from 'prop-types';

const Card = (props) => (
	<div className={props.cardSize ? `card-${props.cardSize}` : "card-large"}>
		{props.children}
	</div>
);

Card.propType = {
	cardSize: PropTypes.string,
}

Card.defaultProps = {
	cardSize: "large",
}

export default Card;


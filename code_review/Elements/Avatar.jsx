
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

class Avatar extends Component {
	constructor(props) {
		super(props);

		this.state = {
      title: this.props.title,
		};
	}

	componentDidMount = () => {
		if (!this.props.title) {
			this.setState({
				title: this.getUserName(),
			});
		}
	}

	getInitials = () => {
		if (this.props.user.full_name) {
			return this.props.user.full_name.substr(0,2).toUpperCase();
		} else if (this.props.user.first_name && this.props.user.last_name) {
			return `${this.props.user.first_name.charAt(0).toUpperCase()} ${this.props.user.last_name.charAt(0).toUpperCase()}`;
		} else if (this.props.user.email) {
			return this.props.user.email.substr(0,2).toUpperCase();
		} else {
			return;
		}
	}

	getUserName = () => {
		if (this.props.user.full_name) {
			return `${this.props.user.full_name}`;
		} else if (this.props.user.first_name && this.props.user.last_name) {
			return `${this.props.user.first_name} ${this.props.user.last_name}`;
		} else if (this.props.user.email) {
			return this.props.user.email;
		} else {
			return 'User';
		}
	}

	getAvatar = () => {
		let spanClass = 'sq-owner-image';
		let avatarContent;

		if (!this.props.user || !this.props.thumbnail || this.props.thumbnail.indexOf('/missing') > -1 ) {
			spanClass = 'blue-bg responsive-circle text-center';

			if (!this.props.user) {
				avatarContent = <span className="missing-owner">N/A</span>;
			} else {
				avatarContent = this.getInitials();
			}

		} else {
			avatarContent = (
				<img
					alt={this.getInitials()}
					className="img-circle img-responsive"
					src={this.props.thumbnail}
				/>
			);
		}

		let tooltip = (
			<Tooltip
				id={`avatarTooltip${this.props.user ? this.props.user.id : Math.floor(Math.random() * (50 - 1) + 1)}`}
				className="avatar-tooltip bridge-tooltip"
			>
				<div className="task-owner">
					<p className="user-name">{this.state.title}</p>
				</div>
			</Tooltip>
		);

		if (this.props.url) {
			return(
				<a
					href={this.props.url}
					aria-label={this.state.title}
				>
					<OverlayTrigger
					  placement="bottom"
					  overlay={tooltip}
					>
						<span className={spanClass}>
							{avatarContent}
						</span>
					</OverlayTrigger>
				</a>
			);
		} else {
			return(
				<OverlayTrigger
				  placement="bottom"
				  overlay={tooltip}
				>
					<span className={spanClass}>
						{avatarContent}
					</span>
				</OverlayTrigger>
			);
		}
	}

	render() {
		let avatar = this.getAvatar();

		return(
			<div className={`avatar ${this.props.customClass ? this.props.customClass : ''} ${this.props.size ? `avatar-${this.props.size}` : ''}`}>
				{avatar}
			</div>
		);
	}

}

Avatar.propTypes = {
	url: PropTypes.string,
	user: PropTypes.object,
	thumbnail: PropTypes.string,
	customClass: PropTypes.string,
	title: PropTypes.string,
	size: PropTypes.oneOf(['sm', 'lg', 'xl']),
};

Avatar.defaultProps = {
	url: null,
	user: {},
	thumbnail: null,
  customClass: '',
  title: '',
  size: 'lg',
};

export default Avatar;

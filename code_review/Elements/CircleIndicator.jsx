
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Elements.css';

// Parent element must have position:relative for correct badge placement
const CircleIndicator = (props) => (
	<div className={`circle-badge badge-sm badge-${props.color}`} />
);

CircleIndicator.propTypes = {
	color: PropTypes.oneOf([
    'primary',
    'warning'
  ]),
};

CircleIndicator.defaultProps = {
  color: 'primary',
};

export default CircleIndicator;

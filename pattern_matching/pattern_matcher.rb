# Pattern Matcher Solution.

class PatternMatcher
    attr_accessor :pattern, :input, :bool, :regex

    def initialize (pattern, input)
        @pattern = pattern
        @input = input
        @bool = nil
        @regex = ''
    end

    def matches
        edge_cases
    end

    # if not an edge case, call get_regex
    # returns true for empty inputs
    def edge_cases
        if @pattern == @input
            @bool = true
            return @bool
        end
        if @pattern == '' && @input == ''
            @bool = true
            return @bool
        end
        get_regex
    end


    # Solution Explanation:
    # FIRST: pattern -> regex string: for pattern given replace
    # first occurence of letter with: (.+) and
    # any time a letter repeats find number N of unique
    # letters in the pattern that appeared before that
    # letter and append \\N to regex string
    # ex: abab -> (.+)(.+)\\1\\2
    # SECOND: evaluate if regex matches input

    def get_regex
        temp = ''
        counts = {}
        @pattern.each_char do |char|
            if !counts.key?(char)
                counts[char] = counts.length + 1
                temp += '(.+)'
            else
                temp += "\\#{counts[char]}"
            end
        end
        @regex_str = temp
        return evaluate
    end

    def evaluate
        @bool = !!Regexp.new(@regex_str).match(@input) #cast to bool value
        return @bool
    end
end


# Quick Test Run
# Alternative would be unit tests

# sample testing data from instructions
test_data_readme = [['abab','redblueredblue',true], ['aaaa','asdasdasdasd',true],['aabb','xyzabcxzyabc',false]]
# extra testing data
test_data_extra = [['', '', true], ['abab', 'abab', true], ['abcdabcd', 'onetwothreefour', false], ['abcdabcd', 'onetwothreefouronetwothreefour', true]]

test_data_readme.each do  |test|
    if PatternMatcher.new(test[0],test[1]).matches == test[2]
        p "Success!"
    else
        p "Fail"
    end
end

test_data_extra.each do  |test|
    if PatternMatcher.new(test[0],test[1]).matches == test[2]
        p "Success!"
    else
        p "Fail"
    end
end


